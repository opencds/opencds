## 2024-08
- [ ] why is Package.packageId minOccurs=0 ?
  - (SupportingData, KnowledgeModule)
- [ ] JWT/JWK work
  - nimbus-jose-jwt supports what we need here
  - understand how a CDS Client creates a JWK that can be trusted by the CDS Service
  - [ ] trusted JKU (list) and ISS
- [ ] sqlite with Jakarta Data
  https://kmm-app-d02.med.utah.edu:28443/opencds-hooks-canned/r4/hooks/cds-services/canned-response-order-select
  bba2e44e-50cd-4c82-b538-f2d74b8842ab
