package org.opencds.config.api.model;

public enum AccessType {
    ALLOW,
    DENY
}
